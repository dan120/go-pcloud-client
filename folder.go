package pcloud

import (
	"errors"
	"fmt"
	"log/slog"
	"net/url"
)

func (p PcloudClient) ListFolder(path string) (*MetadataResponse, error) {
	var response MetadataResponse

	slog.Debug("pcloud.ListFolder", "path", path)

	// perform HTTP call
	err := p.doHttpGet(fmt.Sprintf("%s/listfolder?path=%s&nofiles=1&noshares=1&auth=%s", p.UrlPrefix, path, p.ApiKey), true, &response)
	if err != nil {
		return nil, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return nil, errors.New(response.Error)
	}

	return &response, nil
}

func (p PcloudClient) CreateFolder(parentFolderId int64, name string) (folderId int64, er error) {
	var response MetadataResponse

	slog.Debug("pcloud.CreateFolder", "parentFolderId", parentFolderId, "name", name)

	ename := url.QueryEscape(name)

	// perform HTTP call
	err := p.doHttpGet(fmt.Sprintf("%s/createfolder?folderid=%d&name=%s&auth=%s", p.UrlPrefix, parentFolderId, ename, p.ApiKey), true, &response)
	if err != nil {
		return 0, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return 0, errors.New(response.Error)
	}

	// return created folder ID
	slog.Debug("Created folder", "name", response.Metadata.Name, "folderId", response.Metadata.FolderId, "parentFolderId", response.Metadata.ParentFolderId)
	return response.Metadata.FolderId, nil
}

func (p PcloudClient) CreateFolderIfNotExists(parentFolderId int64, name string) (folderId int64, created bool, er error) {
	var response MetadataResponse

	slog.Debug("pcloud.CreateFolderIfNotExists", "parentFolderId", parentFolderId, "name", name)

	ename := url.QueryEscape(name)

	// perform HTTP call
	err := p.doHttpGet(fmt.Sprintf("%s/createfolderifnotexists?folderid=%d&name=%s&auth=%s", p.UrlPrefix, parentFolderId, ename, p.ApiKey), true, &response)
	if err != nil {
		return 0, false, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return 0, false, errors.New(response.Error)
	}

	// return created folder ID
	slog.Debug("Created folder", "name", response.Metadata.Name, "folderId", response.Metadata.FolderId, "parentFolderId", response.Metadata.ParentFolderId)
	return response.Metadata.FolderId, response.Created, nil
}
