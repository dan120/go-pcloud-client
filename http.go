package pcloud

import (
	"encoding/json"
	"io"
	"net/http"
)

func (p PcloudClient) doHttpGet(url string, keepAlive bool, response interface{}) error {
	// make HTTP call
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	if keepAlive {
		req.Header.Add("Connection", "keep-alive")
	}
	resp, err := p.Client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(body, response); err != nil {
		return err
	}
	return nil
}
