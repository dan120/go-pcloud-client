package pcloud

import (
	"context"
	"net/http"
)

type PcloudClient struct {
	Context   context.Context
	UrlPrefix string
	ApiKey    string
	Client    *http.Client
}

func NewPcloudClient(ctx context.Context, urlPrefix string, apiKey string, client *http.Client) PcloudClient {
	return PcloudClient{
		Context:   ctx,
		UrlPrefix: urlPrefix,
		ApiKey:    apiKey,
		Client:    client,
	}
}
