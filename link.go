package pcloud

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type CreateUploadLinkResponse struct {
	Result       int64  `json:"result"`
	UploadLinkId int64  `json:"uploadlinkid"`
	Link         string `json:"link"`
	Mail         string `json:"mail"`
	Code         string `json:"code"`
}

type PubLinkResponse struct {
	Result int64  `json:"result"`
	LinkId int64  `json:"linkid"`
	Link   string `json:"link"`
	Code   string `json:"code"`
}

func (p PcloudClient) CreateUploadLink(folderId int64) (int64, string, error) {
	// make create upload link call
	resp, err := http.Get(fmt.Sprintf("%s/createuploadlink?folderid=%d&comment=a8preservation.com_upload_request&auth=%s", p.UrlPrefix, folderId, p.ApiKey))
	if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}
	var createUploadLinkResponse CreateUploadLinkResponse
	if err := json.Unmarshal(body, &createUploadLinkResponse); err != nil {
		return 0, "", err
	}

	// verify that result is ok
	if createUploadLinkResponse.Result != 0 {
		return 0, "", fmt.Errorf("failed to create folder with status %d", createUploadLinkResponse.Result)
	}

	return createUploadLinkResponse.UploadLinkId, createUploadLinkResponse.Link, nil
}

func (p PcloudClient) GetFilePubLink(fileId int64) (linkId int64, link string, err error) {
	// make create folder call
	resp, err := http.Get(fmt.Sprintf("%s/getfilepublink?fileid=%d&auth=%s", p.UrlPrefix, fileId, p.ApiKey))
	if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}
	var pubLinkResponse PubLinkResponse
	if err := json.Unmarshal(body, &pubLinkResponse); err != nil {
		return 0, "", err
	}

	// verify that result is ok
	if pubLinkResponse.Result != 0 {
		return 0, "", fmt.Errorf("failed to create public link with status %d", pubLinkResponse.Result)
	}

	return pubLinkResponse.LinkId, pubLinkResponse.Link, nil
}
