package pcloud

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindFileRecursively(t *testing.T) {
	client := NewPcloudClient(context.Background(), "", "", nil)

	body := []byte("{\"result\":0,\"metadata\":{\"name\":\"c31c2c11-7559-482b-9f13-df88da4d2cba\",\"created\":\"Wed, 08 Nov 2023 20:18:18 +0000\",\"ismine\":true,\"thumb\":false,\"modified\":\"Sat, 11 Nov 2023 14:43:14 +0000\",\"comments\":0,\"id\":\"d8465010058\",\"isshared\":false,\"icon\":\"folder\",\"isfolder\":true,\"parentfolderid\":7078832834,\"folderid\":8465010058,\"contents\":[{\"name\":\"1 file from AtariGrub on Nov 11, 2023\",\"created\":\"Sat, 11 Nov 2023 14:43:14 +0000\",\"ismine\":true,\"thumb\":false,\"modified\":\"Sat, 11 Nov 2023 14:43:14 +0000\",\"comments\":0,\"id\":\"d8497373637\",\"isshared\":false,\"icon\":\"folder\",\"isfolder\":true,\"parentfolderid\":8465010058,\"folderid\":8497373637,\"contents\":[{\"name\":\"processed.zip\",\"created\":\"Sat, 11 Nov 2023 14:45:22 +0000\",\"thumb\":false,\"modified\":\"Sat, 11 Nov 2023 14:45:22 +0000\",\"isfolder\":false,\"fileid\":34626360168,\"hash\":16760539703471150163,\"comments\":0,\"category\":5,\"id\":\"f34626360168\",\"isshared\":false,\"ismine\":true,\"size\":45536833,\"parentfolderid\":8497373637,\"contenttype\":\"application/zip\",\"icon\":\"archive\"}]},{\"name\":\"1 file from Farb on Nov 8, 2023\",\"created\":\"Wed, 08 Nov 2023 20:18:39 +0000\",\"ismine\":true,\"thumb\":false,\"modified\":\"Wed, 08 Nov 2023 20:18:39 +0000\",\"comments\":0,\"id\":\"d8465012711\",\"isshared\":false,\"icon\":\"folder\",\"isfolder\":true,\"parentfolderid\":8465010058,\"folderid\":8465012711,\"contents\":[{\"name\":\"raw.zip\",\"created\":\"Wed, 08 Nov 2023 20:18:43 +0000\",\"thumb\":false,\"modified\":\"Wed, 08 Nov 2023 20:18:43 +0000\",\"isfolder\":false,\"fileid\":34498658937,\"hash\":18256981984714794064,\"comments\":0,\"category\":5,\"id\":\"f34498658937\",\"isshared\":false,\"ismine\":true,\"size\":52097791,\"parentfolderid\":8465012711,\"contenttype\":\"application/zip\",\"icon\":\"archive\"}]}]}}")
	var listFolderResponse MetadataResponse
	err := json.Unmarshal(body, &listFolderResponse)
	assert.Nil(t, err)
	f := client.FindFileRecursively("processed.zip", &listFolderResponse.Metadata.Contents)
	assert.NotNil(t, f)
	assert.True(t, len(f) > 0)
	f = client.FindFileRecursively("processed2.zip", &listFolderResponse.Metadata.Contents)
	assert.Nil(t, f)
	assert.True(t, len(f) == 0)
	f = client.FindFileRecursively("raw.zip", &listFolderResponse.Metadata.Contents)
	assert.NotNil(t, f)
	assert.True(t, len(f) > 0)
}

func TestFindNewestFileWithNoFiles(t *testing.T) {
	client := NewPcloudClient(context.Background(), "", "", nil)

	f, err := client.FindNewestFile(nil)
	assert.Nil(t, f)
	assert.Nil(t, err)

	var files []Metadata = make([]Metadata, 0)
	f, err = client.FindNewestFile(files)
	assert.Nil(t, f)
	assert.Nil(t, err)
}

func TestFindNewestFileWithValidFiles(t *testing.T) {
	client := NewPcloudClient(context.Background(), "", "", nil)

	var files []Metadata = make([]Metadata, 3)
	files[0] = Metadata{
		Name:    "oldest.zip",
		Created: "Wed, 08 Nov 2023 08:18:18 +0000",
	}
	files[1] = Metadata{
		Name:    "newest.zip",
		Created: "Thu, 09 Nov 2023 20:18:18 +0000",
	}
	files[2] = Metadata{
		Name:    "middle.zip",
		Created: "Wed, 08 Nov 2023 20:18:18 +0000",
	}

	file, err := client.FindNewestFile(files)
	assert.Equal(t, 3, len(files))
	assert.Nil(t, err)
	assert.NotNil(t, file)
	assert.Equal(t, "newest.zip", file.Name)
}

func TestFindNewestFileWithValidFiles2(t *testing.T) {
	client := NewPcloudClient(context.Background(), "", "", nil)

	var files []Metadata = make([]Metadata, 3)
	files[0] = Metadata{
		Name:    "oldest.zip",
		Created: "Wed, 03 Jan 2024 18:42:21 +0000",
	}
	files[1] = Metadata{
		Name:    "newest.zip",
		Created: "Sun, 07 Jan 2024 15:41:05 +0000",
	}
	files[2] = Metadata{
		Name:    "middle.zip",
		Created: "Sat, 06 Jan 2024 13:57:17 +0000",
	}

	file, err := client.FindNewestFile(files)
	assert.Equal(t, 3, len(files))
	assert.Nil(t, err)
	assert.NotNil(t, file)
	assert.Equal(t, "newest.zip", file.Name)
}

func TestFindNewestFileWithNoCreateTime(t *testing.T) {
	client := NewPcloudClient(context.Background(), "", "", nil)

	var files []Metadata = make([]Metadata, 3)
	files[0] = Metadata{
		Name: "oldest.zip",
	}
	files[1] = Metadata{
		Name: "newest.zip",
	}
	files[2] = Metadata{
		Name: "middle.zip",
	}

	file, err := client.FindNewestFile(files)
	assert.Equal(t, 3, len(files))
	assert.Nil(t, err)
	assert.Nil(t, file)
}
