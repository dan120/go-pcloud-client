package pcloud

import (
	"crypto/sha1"
	"encoding/hex"
	"io"
	"os"
)

func CreateSHA1Hash(filePath string) (string, error) {
	// initialize variable returnMD5String now in case an error has to be returned
	var returnSHA1String string

	// open the filepath passed by the argument and check for any error
	file, err := os.Open(filePath)
	if err != nil {
		return returnSHA1String, err
	}
	defer file.Close()

	// open a new SHA1 hash interface to write to
	hash := sha1.New()

	// copy the file in the hash interface and check for any error
	if _, err := io.Copy(hash, file); err != nil {
		return returnSHA1String, err
	}

	// get the 20 bytes hash
	hashInBytes := hash.Sum(nil)[:20]

	// convert the bytes to a string
	returnSHA1String = hex.EncodeToString(hashInBytes)

	return returnSHA1String, nil
}
