package pcloud

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"strings"
)

type FileOpenResponse struct {
	Result int64  `json:"result"`
	FD     int64  `json:"fd"`
	FileId int64  `json:"fileid"`
	Error  string `json:"error"`
}

type FileSizeResponse struct {
	Result int64 `json:"result"`
	Size   int64 `json:"size"`
	Offset int64 `json:"offset"`
}

type FileCloseResponse struct {
	Result int64 `json:"result"`
}

func (p PcloudClient) FileOpen(fileId int64) (*FileOpenResponse, error) {
	var response FileOpenResponse

	// make open file call
	url := fmt.Sprintf("%s/file_open?flags=0&fileid=%d&auth=%s", p.UrlPrefix, fileId, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("error creating request context for %s: %v", url, err)
	}
	req.Header.Add("Connection", "keep-alive")
	resp, err := p.Client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error calling %s: %v", url, err)
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body from %s: %v", url, err)
	}
	if err := json.Unmarshal(body, &response); err != nil {
		return nil, fmt.Errorf("error unmarshaling response body from %s: %v", url, err)
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return nil, fmt.Errorf("error response from %s: %v", url, response.Error)
	}

	return &response, nil
}

func (p PcloudClient) FileSize(fd int64) (int64, error) {
	// make file pread call
	url := fmt.Sprintf("%s/file_size?fd=%d&auth=%s", p.UrlPrefix, fd, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return 0, err
	}
	req.Header.Add("Connection", "keep-alive")
	resp, err := p.Client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	// read the full response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	var response FileSizeResponse
	if err := json.Unmarshal(body, &response); err != nil {
		return 0, err
	}

	if response.Offset != 0 {
		return 0, fmt.Errorf("got unexpected offset: %d", response.Offset)
	}

	if response.Result != 0 {
		return 0, fmt.Errorf("got error response: %d", response.Result)
	}

	return response.Size, nil
}

func (p PcloudClient) FilePread(fd int64, count int64, offset int64) ([]byte, error) {
	// make file pread call
	url := fmt.Sprintf("%s/file_pread?fd=%d&count=%d&offset=%d&auth=%s", p.UrlPrefix, fd, count, offset, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Connection", "keep-alive")
	resp, err := p.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// read the full response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// if we get a JSON response, it was an error
	contentType := resp.Header.Get("Content-Type")
	if strings.HasPrefix(contentType, "application/json") {
		var response MetadataResponse
		err := json.Unmarshal(body, &response)
		if err != nil {
			slog.Error("Failed to unmarshal error JSON", "error", err, "json", string(body))
			return nil, fmt.Errorf("unexpected failure unmarshaling error JSON: %v", err)
		} else {
			return nil, errors.New(response.Error)
		}
	}

	return body, nil
}

func (p PcloudClient) FileClose(fd int64) {
	url := fmt.Sprintf("%s/file_close?fd=%d&auth=%s", p.UrlPrefix, fd, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		slog.Error("error closing file", "error", err)
	}
	resp, err := p.Client.Do(req)
	if err != nil {
		slog.Error("error closing file", "error", err)
	}
	defer resp.Body.Close()

	// read the full response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		slog.Error("error closing file", "error", err)
	}
	var response FileCloseResponse
	if err := json.Unmarshal(body, &response); err != nil {
		slog.Error("error closing file", "error", err)
	}
	if response.Result != 0 {
		slog.Error("error closing file", "result", response.Result)
	}
}
