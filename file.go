package pcloud

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

type UploadFileResponse struct {
	Result    int64       `json:"result"`
	Metadata  []Metadata  `json:"metadata"`
	FileIDs   []int64     `json:"fileids"`
	Checksums []Checksums `json:"checksums"`
	Error     string      `json:"error"`
}

type Checksums struct {
	SHA1   string `json:"sha1"`
	SHA256 string `json:"sha256"`
}

func (p PcloudClient) UploadFileLocal(filePath string, targetFolderId int64) error {
	var response UploadFileResponse

	filename := url.QueryEscape(path.Base(filePath))

	slog.Debug("pcloud.UploadFile", "filePath", filePath, "fileName", filename, "targetFolderId", targetFolderId)

	// open the local file for reading
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	url := fmt.Sprintf("%s/uploadfile?folderid=%d&nopartial=1&auth=%s", p.UrlPrefix, targetFolderId, p.ApiKey)

	// create the form multi-part
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(filePath))
	if err != nil {
		return err
	}

	// copy the file data to the part
	_, err = io.Copy(part, file)
	if err != nil {
		return err
	}

	// close the writer
	err = writer.Close()
	if err != nil {
		return err
	}

	// create the HTTP POST request
	request, err := http.NewRequest("POST", url, body)
	if err != nil {
		return err
	}
	request.Header.Add("Content-Type", writer.FormDataContentType())

	// perform the request
	resp, err := p.Client.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// read the response
	jbody, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// convert response to JSON
	if err := json.Unmarshal(jbody, &response); err != nil {
		return err
	}

	// check that one file was acknowledged
	if len(response.FileIDs) != 1 {
		return fmt.Errorf("received unexpected number of file IDs: %d", len(response.FileIDs))
	}

	// verify file checksum of the uploaded file
	sha1, err := CreateSHA1Hash(filePath)
	if err != nil {
		return err
	}
	if response.Checksums[0].SHA1 != sha1 {
		return fmt.Errorf("checksum of uploaded file does not match: local=%s vs. remote=%s", sha1, response.Checksums[0].SHA1)
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return errors.New(response.Error)
	}

	return nil
}

func (p PcloudClient) DownloadFileLocal(fileId int64, localFile string) error {
	// open the file for reading
	fileOpenResponse, err := p.FileOpen(fileId)
	if err != nil {
		return fmt.Errorf("error opening file: %v", err)
	}
	defer p.FileClose(fileOpenResponse.FD)

	// get file size
	size, err := p.FileSize(fileOpenResponse.FD)
	if err != nil {
		return fmt.Errorf("error retrieving file size: %v", err)
	}
	slog.Debug("File size retrieved", "fileId", fileId, "size", size, "fid", fileOpenResponse.FD)

	// create local file for output
	fo, err := os.Create(localFile)
	if err != nil {
		return fmt.Errorf("error creating local file for output: %v", err)
	}
	defer func() {
		if err := fo.Close(); err != nil {
			slog.Error("Error closing local file: %v", err)
		}
	}()

	// read pCloud file to local file
	b, err := p.FilePread(fileOpenResponse.FD, size, 0)
	if err != nil {
		return fmt.Errorf("error reading remote file: %v", err)
	}

	// write to local file
	wrote, err := fo.Write(b)
	if err != nil {
		return fmt.Errorf("error writing to local file: %v", err)
	}
	if int64(wrote) != size {
		return fmt.Errorf("unable to write entire file; only wrote %d/%d bytes", wrote, size)
	}

	return nil
}

func (p PcloudClient) FindFile(folderId int64, filename string) ([]Metadata, error) {
	var response []Metadata

	// make list folder call
	resp, err := http.Get(fmt.Sprintf("%s/listfolder?folderid=%d&recursive=1&auth=%s", p.UrlPrefix, folderId, p.ApiKey))
	if err != nil {
		return response, err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return response, err
	}
	var listFolderResponse MetadataResponse
	if err := json.Unmarshal(body, &listFolderResponse); err != nil {
		return response, err
	}

	return p.FindFileRecursively(filename, &listFolderResponse.Metadata.Contents), nil
}

func (p PcloudClient) FindFileRecursively(filename string, contents *[]Metadata) []Metadata {
	var foundFiles []Metadata

	filename = strings.ToLower(filename)

	if contents != nil {
		for _, f := range *contents {
			if f.IsFolder {
				ff := p.FindFileRecursively(filename, &f.Contents)
				if len(ff) > 0 {
					foundFiles = append(foundFiles, ff...)
				}
			} else if strings.ToLower(f.Name) == filename {
				foundFiles = append(foundFiles, f)
			}
		}
	}

	return foundFiles
}

func (p PcloudClient) FindNewestFile(files []Metadata) (*Metadata, error) {
	var result *Metadata

	if files != nil {
		var foundTime time.Time
		var foundFile Metadata
		found := false
		for _, f := range files {
			if f.Created != "" {
				t, err := time.Parse(time.RFC1123, f.Created)
				if err != nil {
					return nil, err
				}
				if foundTime.Before(t) {
					foundFile = f
					foundTime = t
					found = true
					slog.Debug("Found newer file", "fileId", f.FileId, "name", f.Name, "date", f.Created)
				}
			}
		}
		if found {
			result = &foundFile
		}
	}

	return result, nil
}
