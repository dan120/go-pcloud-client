package pcloud

type MetadataResponse struct {
	Result   int64    `json:"result"`
	Created  bool     `json:"created"`
	Metadata Metadata `json:"metadata"`
	Error    string   `json:"error"`
}

type Metadata struct {
	Created        string     `json:"created"`
	IsFolder       bool       `json:"isfolder"`
	ParentFolderId int64      `json:"parentfolderid"`
	Icon           string     `json:"icon"`
	Id             string     `json:"id"`
	FileId         int64      `json:"fileid"`
	Path           string     `json:"path"`
	Modified       string     `json:"modified"`
	Thumb          bool       `json:"thumb"`
	FolderId       int64      `json:"folderid"`
	IsShared       bool       `json:"isshared"`
	IsMine         bool       `json:"ismine"`
	Name           string     `json:"name"`
	Size           int64      `json:"size"`
	Contents       []Metadata `json:"contents"`
}
